from feiertageDE import Feiertage

# Beispieleingabe nur für Jahr abholen, Format: int
jahr = int(input("Jahr (YYYY): "))
# Beispieleingabe für Bundesland anhand offiziellem 2-stelligem Kürzel
bula = input("Bundesland (offizielles 2-stelliges Kürzel): ")

# Ausgabe aller Feiertage als Python Dictionary für bestimmtes Jahr und Bundesland, wird Bundesland leer gelassen, werden die Deutschlandweit gültigen Feiertage ausgegeben.
print(Feiertage.alleFeiertage(jahr, bula))

# Ausgabe aller Feiertag im JSON-Format für bestimmtes Jahr und Bundesland, wird Bundesland leer gelassen, werden die Deutschlandweit gültigen Feiertage ausgegeben.
print(Feiertage.alleFeiertage(jahr, bula, True))


# Beispieleingabe für komplettes Datum
datum = input("Datum (DD.MM.YYYY): ")
# Beispieleingabe für Bundesland anhand offiziellem 2-stelligem Kürzel
bula = input("Bundesland (offizielles 2-stelliges Kürzel): ")

# Beispiel für Prüfung, ob eingegebenes Datum im Bundesland ein Feiertag ist, mit Ausgabe True oder False
print(Feiertage.istFeiertag(datum, bula))

# Beispiel für Prüfung, ob eingegebenes Datum im Bundesland ein Feiertag ist, mit Ausgabe des Tags & Datum als Python-Dictionary
print(Feiertage.istFeiertag(datum, bula, True))

# Beispiel für Prüfung, ob eingegebenes Datum im Bundesland ein Feiertag ist, mit Ausgabe des Tags & Datum im JSON-Format
print(Feiertage.istFeiertag(datum, bula, True, True))