import datetime, json

class Feiertage:
    feiertage_datum_fix = {"Neujahr":"01.01",
                           "Heilige drei Könige":"06.01",
                           "Internationaler Frauentag":"08.03",
                           "Tag der Arbeit":"01.05",
                           "Mariä Himmelfahrt":"15.08",
                           "Weltkindertag":"20.09",
                           "Tag der Deutschen Einheit":"03.10",
                           "Reformationstag":"31.10",
                           "Allerheiligen":"01.11",
                           "1. Weihnachtstag":"25.12",
                           "2. Weihnachtstag":"26.12",
                          }

    feiertage_bula_test = { "DE":{"Feiertage Bundesweit":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True,
                                        "Ostersonntag":False, 
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "BW":{"Feiertage Baden-Württemberg":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":True, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":True, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "BY":{"Feiertage Bayern":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":True,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":True, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":True, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "BE":{"Feiertage Berlin":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":True, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "BB":{"Feiertage Brandenburg":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":True,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":True, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "HB":{"Feiertage Bremen":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "HH":{"Feiertage Hamburg":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "HE":{"Feiertage Hessen":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":True, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "MV":{"Feiertage Mecklenburg-Vorpommern":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":True, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "NI":{"Feiertage Niedersachsen":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "NW":{"Feiertage Nordrhein-Westfalen":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":True, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":True, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "RP":{"Feiertage Rheinland-Pfalz":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":True, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":True, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "SL":{"Feiertage Saarland":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":True, 
                                        "Mariä Himmelfahrt":True, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":True, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "SN":{"Feiertage Sachsen":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":True, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":True, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "ST":{"Feiertage Sachsen-Anhalt":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":True,
                                        "Internationaler Frauentag":True, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":True, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "SH":{"Feiertage Schleswig-Holstein":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":False, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":False, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                            "TH":{"Feiertage Thüringen":
                                    {
                                        "Neujahr":True,
                                        "Heilige drei Könige":False,
                                        "Internationaler Frauentag":False, 
                                        "Karfreitag":True, 
                                        "Ostersonntag":False,
                                        "Ostermontag":True, 
                                        "Tag der Arbeit":True, 
                                        "Christi Himmelfahrt":True, 
                                        "Pfingstmontag":True, 
                                        "Fronleichnam":False, 
                                        "Mariä Himmelfahrt":False, 
                                        "Weltkindertag":True, 
                                        "Tag der Deutschen Einheit":True, 
                                        "Reformationstag":True, 
                                        "Allerheiligen":False, 
                                        "Buß- und Bettag":False, 
                                        "1. Weihnachtstag":True, 
                                        "2. Weihnachtstag":True
                                    }
                                },
                          }

    def ostern(jahr): #Gaußsche Osterformel
        A = jahr%19
        K = jahr//100
        M = 15+(3*K+3)//4-(8*K+13)//25
        D = (19*A+M)%30
        S = 2-(3*K+3)//4
        R = D//29+(D//28-D//29)*(A//11)
        OG = 21+D+R
        SZ = 7-(jahr+jahr//4+S)%7
        OE = 7-(OG-SZ)%7
        OS = (OG+OE)
        return(OS)

    def welcherMonat(dat):
        if dat > 31 and dat <= 61:
            return(str(dat-31) + ".04")
        elif dat > 61 and dat <= 92:
            return(str(dat-61) + ".05")
        elif dat > 92:
            return(str(dat-92) + ".06")
        else:
            return(str(dat) + ".03")

    def berechneVariableFeiertage(jahr, tagohnedatum):
        ostertag = Feiertage.ostern(jahr)

        if tagohnedatum.__contains__("Christi Himmelfahrt"):
            tag = Feiertage.welcherMonat(ostertag + 39)
        elif tagohnedatum.__contains__("Ostermontag"):
            tag = Feiertage.welcherMonat(ostertag + 1)
        elif tagohnedatum.__contains__("Karfreitag"):
            tag = Feiertage.welcherMonat(ostertag - 2)
        elif tagohnedatum.__contains__("Pfingstmontag"):
            tag = Feiertage.welcherMonat(ostertag + 50)
        elif tagohnedatum.__contains__("Fronleichnam"):
            tag = Feiertage.welcherMonat(ostertag + 60)
        elif tagohnedatum.__contains__("Buß- und Bettag"):
            weihnacht = datetime.date(jahr,12,25)
            differenz = 0
            vortag = weihnacht
            wochentag = weihnacht.weekday()
            while wochentag != 6 or vortag == weihnacht:
                vortag = (vortag - datetime.timedelta(days = 1))
                wochentag = vortag.weekday()
                differenz += 1
            bettag = weihnacht - datetime.timedelta(days = differenz+32)
            tag = str(bettag)[8:10] + "." + str(bettag)[5:7]
        else:
            tag = Feiertage.welcherMonat(ostertag)
        return(tag)

    @classmethod
    def alleFeiertage(cls, jahr, bula = "DE", alsjson=False):
        ftBula = Feiertage.feiertage_bula_test[bula]

        for key, value in ftBula.items():
            nftBula = value.copy()
            for key2, value2 in value.items():
                if value2 == False:
                    del nftBula[key2]
                else:
                    try:
                        nval = Feiertage.feiertage_datum_fix[key2]
                        del nftBula[key2]
                        nftBula[key2] = nval
                    except Exception as err:
                        tagohnedatum = str(err)
                        del nftBula[key2]
                        tag = Feiertage.berechneVariableFeiertage(jahr, tagohnedatum)
                        if len(tag) <= 4:
                            tag = "0" + tag
                        nftBula[key2] = tag
            value.clear()
            value.update(nftBula)

        if alsjson:
            return(json.dumps(ftBula, indent=4))
        else:
            return(ftBula)

    @classmethod
    def istFeiertag(cls, datum, bula, tagAusgeben = False, alsjson=False):
        jahr = int(datum[6:10])
        feiertage = Feiertage.alleFeiertage(jahr, bula)
        for key, value in feiertage.items():
            for key2, value2 in value.items():
                if datum[0:5] == value2:
                    if tagAusgeben:
                        if alsjson:
                            tag = [True, key2, value2]
                            return(json.dumps(tag, indent=4))
                        else:
                            return(True, key2, value2)
                    else:
                        return(True)
                    break
        return(False)